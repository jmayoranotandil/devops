#!/bin/bash

# permissions
if [ "$(whoami)" != "root" ]; then
    echo "Root privileges are required to run this, try running with sudo..."
    exit 2
fi

documentRoot='';
project='';
apacheDir='/etc/apache2/sites-available/';

for i in "$@"
do
    case $i in
        -r=*|--document_root=*)
        documentRoot="${i#*=}"
        shift # past argument=value
        ;;
        -p=*|--project=*)
        project="${i#*=}"
        shift # past argument=value
        ;;
        -d=*|--domain=*)
        domain="${i#*=}"
        shift # past argument=value
        ;;
        -a=*|--apache_dir=*)
        apacheDir="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac;
done;

if [[ -z "$documentRoot" ]]; then
    echo "No document root specified."
    exit 2
fi;

if [[ -z "$project" ]]; then
    echo "No project specified."
    exit 2
fi;

vhostFilename="$project".conf;
curl -o "$vhostFilename" http://summalibrary.summasolutions.net/snippets/apache-magento-vhost/raw?document_root="$documentRoot"\&domain="$domain";

mv "$vhostFilename" "$apacheDir";
a2ensite $vhostFilename;
service apache2 restart;

if ! grep -q "127.0.0.1 $domain" /etc/hosts; then
    echo 'Adding entry to /etc/hosts file';
    sed -i -e '$a\' /etc/hosts;
    echo "127.0.0.1 $domain" >> /etc/hosts;
fi;

exit 0;
