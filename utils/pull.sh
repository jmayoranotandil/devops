#!/bin/bash

if [ -z "$s1" ]
    then
        echo "Pulling code for $1"
        echo 'git fetch -q --all';
        git fetch -q --all;
        echo "git checkout $1"
        git checkout -q $1;
        echo "git pull -q origin $1";
        git pull -q origin $1;

    else
        echo 'No branch or tag specified';
fi;