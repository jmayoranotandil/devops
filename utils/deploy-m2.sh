#!/bin/bash

BASIC_AUTH_USERNAME='';
BASIC_AUTH_PASSWORD='';
DEPLOY_HISTORY_FILES=5;
RUN_UPGRADE=false;
STATIC_CONTENT_PARAMETERS='';

for i in "$@"
do
    case $i in
        -b=*|--build_name=*)
        BUILD_NAME="${i#*=}"
        shift # past argument=value
        ;;
        -r=*|--deploy_root_dir=*)
        DEPLOY_ROOT_DIR="${i#*=}"
        shift # past argument=value
        ;;
        -f=*|--deploy_history_files=*)
        DEPLOY_HISTORY_FILES="${i#*=}"
        shift # past argument=value
        ;;
        -w=*|--webroot_folder=*)
        WEBROOT_FOLDER="${i#*=}"
        shift # past argument=value
        ;;
        -h=*|--host_url=*)
        HOST_URL="${i#*=}"
        shift # past argument=value
        ;;
        -u=*|--basic_auth_username=*)
        BASIC_AUTH_USERNAME="${i#*=}"
        shift # past argument=value
        ;;
        -p=*|--basic_auth_password=*)
        BASIC_AUTH_PASSWORD="${i#*=}"
        shift # past argument=value
        ;;
        -x=*|--deploy_run_upgrade=*)
        RUN_UPGRADE="${i#*=}"
        shift # past argument=value
        ;;
        -s=*|--additional_sh_script=*)
        ADDITIONAL_SH_SCRIPT="${i#*=}"
        shift # past argument=value
        ;;
        -c=*|--static_content_parameters=*)
        STATIC_CONTENT_PARAMETERS="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac;
done;

echo "Generating symlinks to shared resource for release $BUILD_NAME";
cd $DEPLOY_ROOT_DIR/shared;

# diff -rq $DEPLOY_ROOT_DIR/shared/ $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/
# This command throws the differences (missing files) between the two dirs.

# grep -F " $DEPLOY_ROOT_DIR/shared/"
# We want the files that are only in the shared lib (these ones are the ones missing in the webroot folder)

# sed "s/: /\//g"
# We replace ": " with "/". This is because of the output of the diff command.

# sed "s/\/\//\//g"
# We replace "//" with "/".

# sed "s~^.*$DEPLOY_ROOT_DIR/shared/~~g"
# And finally, we replace all the starting path that we do not need (we just need the relative path from shared folder)

# tr "\n" "\0"
# We replace "\n" with "\0", this is the suppport for filenames with spaces.

diff -rq $DEPLOY_ROOT_DIR/shared/ $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/ | grep -F " $DEPLOY_ROOT_DIR/shared/" | sed "s/: /\//g" | sed "s/\/\//\//g" | sed "s~^.*$DEPLOY_ROOT_DIR/shared/~~g" | tr "\n" "\0" | while IFS= read -r -d $'\0' file; do
    ln -s "$DEPLOY_ROOT_DIR/shared/$file" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$file";
    echo "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$file symlink created pointing to $DEPLOY_ROOT_DIR/shared/$file";
done;

echo "Pointing $WEBROOT_FOLDER folder to release $BUILD_NAME";
rm -f $DEPLOY_ROOT_DIR/$WEBROOT_FOLDER;
ln -sf $DEPLOY_ROOT_DIR/releases/$BUILD_NAME $DEPLOY_ROOT_DIR/$WEBROOT_FOLDER;

# find -L $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER -name . -o -type d -prune -o -type l -exec rm {} +

# at this point we run any additional sh script passing the very same parameters the we receive.
# we do this before all the cache clearing process.

echo "Additonal Script Path $ADDITIONAL_SH_SCRIPT";
if [[ "$ADDITIONAL_SH_SCRIPT" ]] && [[ -e "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" ]]; then
    echo "Excecuting additional script";
    echo /bin/bash "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" --build_name="$BUILD_NAME" --deploy_root_dir="$DEPLOY_ROOT_DIR" --webroot_folder="$WEBROOT_FOLDER";
    /bin/bash "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" --build_name="$BUILD_NAME" --deploy_root_dir="$DEPLOY_ROOT_DIR" --webroot_folder="$WEBROOT_FOLDER";
fi;

# after the custom script has ran, we can flush all the needed caches, etc.
BASIC_AUTH_PARAMETER='';
if [[ "$BASIC_AUTH_USERNAME" ]] && [[ "$BASIC_AUTH_PASSWORD" ]]; then
    BASIC_AUTH_PARAMETER="--user $BASIC_AUTH_USERNAME:$BASIC_AUTH_PASSWORD";
fi;

echo "Clear OP Codes Cache"
echo ln -s "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/utils/cc_opcache.php" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/pub/cc_opcache.php";
ln -s "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/utils/cc_opcache.php" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/pub/cc_opcache.php";

echo "ATTENTION: Make sure that /etc/hosts file in the server points the IP '127.0.0.1' to $HOST_URL, if not, the following line will fail";
echo "ATTENTION: Also, make sure that you can access $HOST_URL/cc_opcache.php via URL";
echo curl -sS --data "deploy-process=true" $BASIC_AUTH_PARAMETER $HOST_URL/cc_opcache.php;
curl -sS --data "deploy-process=true" $BASIC_AUTH_PARAMETER $HOST_URL/cc_opcache.php;
rm $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/pub/cc_opcache.php;

echo "Removing old builds";
cd $DEPLOY_ROOT_DIR/releases/ && (ls -t|head -n $DEPLOY_HISTORY_FILES;ls)|sort|uniq -u|xargs rm -rf;

echo "Enabling maintenance mode";
echo php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" maintenance:enable;
php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" maintenance:enable;

if [[ "$RUN_UPGRADE" ]]; then
    echo "Running Magento installers";
    echo php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:upgrade;
    php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:upgrade;
fi;

echo "Setting M2 Production Mode";
echo php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:di:compile
php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:di:compile

echo "Touching deployed_version";
echo touch "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER"/pub/static/deployed_version.txt;
touch "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER"/pub/static/deployed_version.txt;

echo chmod -R 777 "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER"/pub/static/deployed_version.txt;
chmod -R 777 "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER"/pub/static/deployed_version.txt;

echo "Running static content deploy";
echo php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:static-content:deploy $STATIC_CONTENT_PARAMETERS;
php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" setup:static-content:deploy $STATIC_CONTENT_PARAMETERS;

echo "Clearing Magento's cache";
echo "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" ca:cl;
php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" ca:cl;

echo "Disabling maintenance mode";
echo php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" maintenance:disable;
php "$DEPLOY_ROOT_DIR"/"$WEBROOT_FOLDER/bin/magento" maintenance:disable;