<?php
if (isset($_POST['deploy-process']) && in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
    if (function_exists('opcache_reset')) {
        opcache_reset();
        echo 'OPCache Cleared.';
    }

    if (function_exists('apc_clear_cache')) {
        apc_clear_cache();
        apc_clear_cache('user');
        apc_clear_cache('opcode');
        echo 'APC Cache Cleared.';

    }

} else {
    echo 'You ware not allowed to run OP Code Cache clear on this server.';
}
