#!/bin/bash

BASIC_AUTH_USERNAME='';
BASIC_AUTH_PASSWORD='';
DEPLOY_HISTORY_FILES=5;
TRIGGER_CC=false;
UPDATE_CSSJS_VERSION=true;

for i in "$@"
do
    case $i in
        -b=*|--build_name=*)
        BUILD_NAME="${i#*=}"
        shift # past argument=value
        ;;
        -r=*|--deploy_root_dir=*)
        DEPLOY_ROOT_DIR="${i#*=}"
        shift # past argument=value
        ;;
        -f=*|--deploy_history_files=*)
        DEPLOY_HISTORY_FILES="${i#*=}"
        shift # past argument=value
        ;;
        -w=*|--webroot_folder=*)
        WEBROOT_FOLDER="${i#*=}"
        shift # past argument=value
        ;;
        -h=*|--host_url=*)
        HOST_URL="${i#*=}"
        shift # past argument=value
        ;;
        -u=*|--basic_auth_username=*)
        BASIC_AUTH_USERNAME="${i#*=}"
        shift # past argument=value
        ;;
        -p=*|--basic_auth_password=*)
        BASIC_AUTH_PASSWORD="${i#*=}"
        shift # past argument=value
        ;;
        -c=*|--deploy_trigger_cc=*)
        TRIGGER_CC="${i#*=}"
        shift # past argument=value
        ;;
        -v=*|--update_cssjs_version=*)
        UPDATE_CSSJS_VERSION="${i#*=}"
        shift # past argument=value
        ;;
        -s=*|--additional_sh_script=*)
        ADDITIONAL_SH_SCRIPT="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac;
done;

echo "Generating symlinks to shared resource for release $BUILD_NAME";
cd $DEPLOY_ROOT_DIR/shared;

# diff -rq $DEPLOY_ROOT_DIR/shared/ $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/
# This command throws the differences (missing files) between the two dirs.

# grep -F " $DEPLOY_ROOT_DIR/shared/"
# We want the files that are only in the shared lib (these ones are the ones missing in the webroot folder)

# sed "s/: /\//g"
# We replace ": " with "/". This is because of the output of the diff command.

# sed "s/\/\//\//g"
# We replace "//" with "/".

# sed "s~^.*$DEPLOY_ROOT_DIR/shared/~~g"
# And finally, we replace all the starting path that we do not need (we just need the relative path from shared folder)

# tr "\n" "\0"
# We replace "\n" with "\0", this is the suppport for filenames with spaces.

diff -rq $DEPLOY_ROOT_DIR/shared/ $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/ | grep -F " $DEPLOY_ROOT_DIR/shared/" | sed "s/: /\//g" | sed "s/\/\//\//g" | sed "s~^.*$DEPLOY_ROOT_DIR/shared/~~g" | tr "\n" "\0" | while IFS= read -r -d $'\0' file; do
    ln -s "$DEPLOY_ROOT_DIR/shared/$file" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/$file";
    echo "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/$file symlink created pointing to $DEPLOY_ROOT_DIR/shared/$file";
done;

echo "Pointing $WEBROOT_FOLDER folder to release $BUILD_NAME";
rm -f $DEPLOY_ROOT_DIR/$WEBROOT_FOLDER;
ln -sf $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER $DEPLOY_ROOT_DIR/$WEBROOT_FOLDER;

# find -L $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER -name . -o -type d -prune -o -type l -exec rm {} +

# at this point we run any additional sh script passing the very same parameters the we receive.
# we do this before all the cache clearing process.

echo "Additonal Script Path $ADDITIONAL_SH_SCRIPT";
if [[ "$ADDITIONAL_SH_SCRIPT" ]] && [[ -e "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" ]]; then
    echo "Excecuting additional script";
    echo /bin/bash "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" --build_name="$BUILD_NAME" --deploy_root_dir="$DEPLOY_ROOT_DIR" --webroot_folder="$WEBROOT_FOLDER";
    /bin/bash "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$ADDITIONAL_SH_SCRIPT" --build_name="$BUILD_NAME" --deploy_root_dir="$DEPLOY_ROOT_DIR" --webroot_folder="$WEBROOT_FOLDER";
fi;

# after the custom script has ran, we can flush all the needd caches, etc.
BASIC_AUTH_PARAMETER='';
if [[ "$BASIC_AUTH_USERNAME" ]] && [[ "$BASIC_AUTH_PASSWORD" ]]; then
    BASIC_AUTH_PARAMETER="--user $BASIC_AUTH_USERNAME:$BASIC_AUTH_PASSWORD";
fi;

echo "Clear OP Codes Cache"
echo ln -s "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/utils/cc_opcache.php" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/cc_opcache.php";
ln -s "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/utils/cc_opcache.php" "$DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/cc_opcache.php";

echo "ATTENTION: Make sure that /etc/hosts file in the server points the IP '127.0.0.1' to $HOST_URL, if not, the following line will fail";
echo curl -sS --data "deploy-process=true" $BASIC_AUTH_PARAMETER $HOST_URL/cc_opcache.php;
curl -sS --data "deploy-process=true" $BASIC_AUTH_PARAMETER $HOST_URL/cc_opcache.php;
rm $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/$WEBROOT_FOLDER/cc_opcache.php;

echo "Removing old builds";
cd $DEPLOY_ROOT_DIR/releases/ && (ls -t|head -n $DEPLOY_HISTORY_FILES;ls)|sort|uniq -u|xargs rm -rf;

if [[ "$UPDATE_CSSJS_VERSION" ]]; then
    echo "Updating CSS/JS version";
    echo php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar -q --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ config:set jscssversion/jscssversion/version $BUILD_NAME;
    php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar -q --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ config:set jscssversion/jscssversion/version $BUILD_NAME;
fi;

if [[ "$TRIGGER_CC" ]]; then
    echo "Clearing Magento Cache";
    echo php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar -q --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ cache:flush;
    php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar -q --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ cache:flush;
fi;

echo "Running Magento installers";
echo php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ sys:setup:run;
php $DEPLOY_ROOT_DIR/releases/$BUILD_NAME/vendor/summasolutions/devops/tools/n98-magerun.phar --root-dir=$DEPLOY_ROOT_DIR/$WEBROOT_FOLDER/ sys:setup:run;