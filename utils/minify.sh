#!/bin/bash

FOLDER_TO_MINIFY='';
MINIFY_TYPE='';
DEVOPS_LOCATION='';

for i in "$@"
do
    case $i in
        -f=*|--folder=*)
        FOLDER_TO_MINIFY="${i#*=}"
        shift # past argument=value
        ;;
        -t=*|--type=*)
        MINIFY_TYPE="${i#*=}"
        shift # past argument=value
        ;;
        -d=*|--devops-location=*)
        DEVOPS_LOCATION="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac;
done;

if [[ "$MINIFY_TYPE" == "js" ]]; then
    find $FOLDER_TO_MINIFY -type f \( -iname '*.js' ! -iname '*.min*' ! -iname '*jquery-ui*' \) -print0 | while IFS= read -r -d $'\0' file; do
        java -jar $DEVOPS_LOCATION/tools/yuicompressor-2.4.9.jar --type js "$file" -o "$file" --preserve-semi 2>/dev/null;
    done;
else
    find $FOLDER_TO_MINIFY -type f -iname '*.css' -print0 | while IFS= read -r -d $'\0' file; do
        java -jar $DEVOPS_LOCATION/tools/yuicompressor-2.4.9.jar --type css "$file" -o "$file" 2>/dev/null;
    done;
fi;
