#!/bin/bash

SITEMAP_URL='';

for i in "$@"
do
    case $i in
        -s=*|--sitemap_url=*)
        SITEMAP_URL="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac;
done;

if [[ "$SITEMAP_URL" ]]; then
    urlsToVisit=`curl -s $SITEMAP_URL | sed 's/\<url\>/\<url\>\n/g' | grep 0.5 | sed 's/.*loc>\(.*\)<\/loc.*/\1/g'`;
    for url in $urlsToVisit; do
        echo "Visiting $url";
        curl -s -o /dev/null $url;
    done;
fi;