# DevOPS task for Summa Magento Projects #

This project contains the files to add build and deploy process to a Magento project.

[TOC]

## Prerequisites ##

**Local folder structure**

You must have installed the latest version of `phing`.

All the scrips inside this project are prepared to work with some specific local Magento folder structure, which is the following

```
repo-root-folder/
    magento-root-folder/
        app/
        skin/
        ...
    other-project-folders/
```

Example:

```
/home/summa/projects/stylewatch/
    webroot/
        app/
        skin/
```

**Server folder structure**

It assumes that the server has the following structure.

```
deploy-folder/
    releases/
    shared/
        media/  ## or pub/media for M2
        var/    ## will not be shared for M2
        app/etc/local.xml ## or app/etc/env.php for M2 
        #any other shared file or folder.
```

Example:

```
/var/www/stylewatch/
    releases/
    shared/
        media/
        var/
        app/etc/local.xml
        #any other shared file or folder.
```

Also, it assumes that the webserver points to `deploy-folder/[magento-root-folder]` (Example `/var/www/stylewatch/webroot` or `/var/www/solodeportes/source`).
This `[magento-root-folder]`, on the deploy, it will be a symbolic link to the proper release. Note that is the very same name as in the local folder structure.

It assumes that you **do not** have a folder inside the repository root named `devops`.
This is because the `build` task will create such folder with utilities for the `deploy` task.

## Installation ##

To install `summasolutions/devops` project you must add this to your `composer.json` file.

```
#!json

{
    "require": {
        "summasolutions/devops": "2.*"
    },
    "repositories": [
        {
            "type": "composer",
            "url": "http://packages.summasolutions.net"
        }
    ],
    "config": {
        "secure-http": false
    }
}
```

Then run 

```
#!bash

composer install # or composer update 
```

Additionally, if you don't want the `devops` folder to mess with your
Magento installation, then simply add it to your .gitignore file.

## Usage ##

These are the commands that you can use at the moment.

* **`build`:** Builds a package ready to be deployed for M1 projects.
* **`build_m2`:** Builds a package ready to be deployed for M2 projects.
* **`build_oro`:** Builds a package ready to be deployed for Oro projects.
* **`deploy`:** Deploys a previously created build for M1 projects.
* **`deploy_m2`:** Deploys a previously created build for M2 projects.
* **`deploy_oro`:** Deploys a previously created build for Oro projects.
* **`pullOnHosts`:** Connect to host and performs a pull
* **`restore_db`:** Brings the database for the project (if properly configured).
* **`setup_environment`:** Configures the db with the proper configuration values for a given environment.
* **`restore_environment`:** Runs `restore_db` and `setup_environment` in that order.
* **`warmup_cache`:** Given a sitemap url, warms up the Magento cache for that site.

All commands must be run as the following (You should be in the repo root folder)

```
#!bash

phing [task] [parameter list]
```

It is important that you run the command in the repository root folder and not inside of any other folder, because some 
paths on the scripts rely on this.

### Parameters ####

There are few parameters that you need to define when you run a taks, those parameters have different name depending on the task.
Here is the list of parameters for each task.

* **Common parameters:**
    * **`webroot.dir`:** _Required_. Indicates the name of the `magento-root-folder`. Some projects uses `source` and some others uses `webroot`.
* **`build` parameters:**
    * **`general.project`:** _Required_. The project name (we recommend use git repo name, example: stylewatch). Needed to feed the Summa Library accordingly.
    * **`build.branch`:** _Required_. Indicates the branch or tag to build.
    * **`build.number`:** _Optional_. Indicates the number of the build, if not specified, the current date will be used, with the following format `%Y%m%d%H%M`.
    * **`build.history.files`:** _Optional_. Indicates how much files should be keept. Defaults to `5`.
    * **`build.use.compass`:** _Optional_. Indicates if compass should be used to compile and compress css files. Defaults to `false`.
    * **`build.use.minification`:** _Optional_. Indicates if CSS and JS minification is going to be applied. Defaults to `false`.
    * **`build.upload.ip`:** _Opional_. Indicates the ip to upload the build to. If this parameter ommited, then there is no need to define any of the others `build.upload.*` parameters.
    * **`build.upload.port`:** _Opional_. Indicates the port to use when connection to `build.upload.ip`.
    * **`build.upload.path`:** _Opional_. Indicates the destination path when uploading the build.
    * **`build.upload.username`:** _Opional_. Indicates the username to use when connecting to `build.upload.ip`.
    * **`build.upload.password`:** _Opional_. Indicates the password to use when connecting to `build.upload.ip`.
    * **`build.upload.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.upload.ip`.
    * **NOTE:** The script is prepared to receive either `build.upload.pem.location` or `build.upload.password`, so if you define the `build.upload.pem.location` you don't need to define the `build.upload.password`.
* **`build_m2` parameters:**
    * **`general.project`:** _Required_. The project name (we recommend use git repo name, example: stylewatch). Needed to feed the Summa Library accordingly.
    * **`build.branch`:** _Required_. Indicates the branch or tag to build.
    * **`build.number`:** _Optional_. Indicates the number of the build, if not specified, the current date will be used, with the following format `%Y%m%d%H%M`.
    * **`build.history.files`:** _Optional_. Indicates how much files should be keept. Defaults to `5`.
    * **`build.upload.ip`:** _Opional_. Indicates the ip to upload the build to. If this parameter ommited, then there is no need to define any of the others `build.upload.*` parameters.
    * **`build.upload.port`:** _Opional_. Indicates the port to use when connection to `build.upload.ip`.
    * **`build.upload.path`:** _Opional_. Indicates the destination path when uploading the build.
    * **`build.upload.username`:** _Opional_. Indicates the username to use when connecting to `build.upload.ip`.
    * **`build.upload.password`:** _Opional_. Indicates the password to use when connecting to `build.upload.ip`.
    * **`build.upload.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.upload.ip`.
    * **NOTE:** The script is prepared to receive either `build.upload.pem.location` or `build.upload.password`, so if you define the `build.upload.pem.location` you don't need to define the `build.upload.password`.
* **`build_oro` parameters:**
    * **`general.project`:** _Required_. The project name (we recommend use git repo name, example: stylewatch). Needed to feed the Summa Library accordingly.
    * **`build.branch`:** _Required_. Indicates the branch or tag to build.
    * **`build.number`:** _Optional_. Indicates the number of the build, if not specified, the current date will be used, with the following format `%Y%m%d%H%M`.
    * **`build.history.files`:** _Optional_. Indicates how much files should be keept. Defaults to `5`.
    * **`build.upload.ip`:** _Opional_. Indicates the ip to upload the build to. If this parameter ommited, then there is no need to define any of the others `build.upload.*` parameters.
    * **`build.upload.port`:** _Opional_. Indicates the port to use when connection to `build.upload.ip`.
    * **`build.upload.path`:** _Opional_. Indicates the destination path when uploading the build.
    * **`build.upload.username`:** _Opional_. Indicates the username to use when connecting to `build.upload.ip`.
    * **`build.upload.password`:** _Opional_. Indicates the password to use when connecting to `build.upload.ip`.
    * **`build.upload.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.upload.ip`.
    * **NOTE:** The script is prepared to receive either `build.upload.pem.location` or `build.upload.password`, so if you define the `build.upload.pem.location` you don't need to define the `build.upload.password`.
* **`deploy` parameters:**
    * **`deploy.path`:** _Required_. Indicates the basedir for the release. This is the folder where to deploy the code. (Eg.: `deploy.path/var/www/stylewatch`) 
    * **`deploy.trigger.cc`:** _Optional_. Indicates if the deploy process should clean the Magento cache or not. Defaults to `false`.
    * **`build.name`:** _Required_. The name generated for a build. You can retrieve if from the `build` task output. It is the generated filename without the `.tar.gz` extensions.
    * **`build.update.cssjs.version`:** _Optional_. Defaults to `true`. Updates the CSS/JS version of the Summa Theme to the latest build being deployed. Set to `false` to avoid the update.
    * **`host.url`:** _Required_. The host url, with the `http://` or `https://` part (Eg.: `host.url=http://www.stylewatch.com.ar`).
        * **`host.basicauth.username`:** _Optional_. The username for the basic auth.
        * **`host.basicauth.password`:** _Optional_. The password for the basic auth.
    * **`environments`:** _Required_. The environments to deploy to, pipe separated (Example: 'web1|web2|web3'). This must be used in combination with the following parameters:
        * **`[env].ip`:** _Required_. The `ip` for the given `env` (Example: `web1.ip=127.0.0.1`). Note that if you defined two environments, you must pass two ips (Example: `web1.ip=127.0.0.1` `web2.ip=127.0.0.2`)
        * **`[env].port`:** _Optional_. The port to use when connection to `[env].ip`.
        * **`[env].username`:** _Required_. The `username` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].password`:** _Optional_. The `password` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].pem.location`:** _Optional_. The location of the pem file to connect to the given `env`. Same as before you need to define as many as environments you have defined.
        * **NOTE:** The script is prepared to receive either `[env].pem.location` or `[env].password`, so if you define the `[env].pem.location` you don't need to define the `[env].password`.
    * **`build.download.ip`:** _Opional_. Indicates the ip to download the build from. If this parameter ommited, then there is no need to define any of the others `build.download.*` parameters.
        * **`build.download.port`:** _Opional_. Indicates the port to use when connecting to `build.download.ip`.
        * **`build.download.path`:** _Opional_. Indicates the path where the build file is located in `build.download.ip`.
        * **`build.download.username`:** _Opional_. Indicates the username to use when connecting to `build.download.ip`.
        * **`build.download.password`:** _Opional_. Indicates the password to use when connecting to `build.download.ip`.
        * **`build.download.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.download.ip`.
        * **NOTE:** The script is prepared to receive either `build.download.pem.location` or `build.download.password`, so if you define the `build.download.pem.location` you don't need to define the `build.download.password`.
    * **`deploy.additional.sh.script`:** _Optional_. Indicate the path to an additional `bash` file to be executed. Path is relative to the `root folder` of the project. The script will be called with the following paremters:
        * **`--build_name`:** The very same parameter passed to the `deploy` task in `build.name`.
        * **`--deploy_root_dir`:** The very same parameter passed to the `deploy` task in `build.path`.
        * **`--webroot_folder`:** The very same parameter passed to the `deploy` task in `webroot.dir`.
* **`deploy_m2` parameters:**
    * **`deploy.path`:** _Required_. Indicates the basedir for the release. This is the folder where to deploy the code. (Eg.: `deploy.path/var/www/stylewatch`) 
    * **`build.name`:** _Required_. The name generated for a build. You can retrieve if from the `build` task output. It is the generated filename without the `.tar.gz` extensions.
    * **`host.url`:** _Required_. The host url, with the `http://` or `https://` part (Eg.: `host.url=http://www.stylewatch.com.ar`).
        * **`host.basicauth.username`:** _Optional_. The username for the basic auth.
        * **`host.basicauth.password`:** _Optional_. The password for the basic auth.
    * **`environments`:** _Required_. The environments to deploy to, pipe separated (Example: 'web1|web2|web3'). This must be used in combination with the following parameters:
        * **`[env].ip`:** _Required_. The `ip` for the given `env` (Example: `web1.ip=127.0.0.1`). Note that if you defined two environments, you must pass two ips (Example: `web1.ip=127.0.0.1` `web2.ip=127.0.0.2`)
        * **`[env].port`:** _Optional_. The port to use when connection to `[env].ip`.
        * **`[env].username`:** _Required_. The `username` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].password`:** _Optional_. The `password` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].pem.location`:** _Optional_. The location of the pem file to connect to the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].run.upgrade`:** _Optional_. Indicates if the deploy process should run the `setup:upgrade` command on that particular env. Defaults to `false`.
        * **NOTE:** The script is prepared to receive either `[env].pem.location` or `[env].password`, so if you define the `[env].pem.location` you don't need to define the `[env].password`.
    * **`build.download.ip`:** _Opional_. Indicates the ip to download the build from. If this parameter ommited, then there is no need to define any of the others `build.download.*` parameters.
        * **`build.download.port`:** _Opional_. Indicates the port to use when connecting to `build.download.ip`.
        * **`build.download.path`:** _Opional_. Indicates the path where the build file is located in `build.download.ip`.
        * **`build.download.username`:** _Opional_. Indicates the username to use when connecting to `build.download.ip`.
        * **`build.download.password`:** _Opional_. Indicates the password to use when connecting to `build.download.ip`.
        * **`build.download.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.download.ip`.
        * **NOTE:** The script is prepared to receive either `build.download.pem.location` or `build.download.password`, so if you define the `build.download.pem.location` you don't need to define the `build.download.password`.
    * ** `deploy.static.content.parameters`:** _Optional_. Parameters for the `setup:static-content:deploy` Magento command.
    * **`deploy.additional.sh.script`:** _Optional_. Indicate the path to an additional `bash` file to be executed. Path is relative to the `root folder` of the project. The script will be called with the following paremters:
        * **`--build_name`:** The very same parameter passed to the `deploy` task in `build.name`.
        * **`--deploy_root_dir`:** The very same parameter passed to the `deploy` task in `build.path`.
        * **`--webroot_folder`:** The very same parameter passed to the `deploy` task in `webroot.dir`.
* **`deploy_oro` parameters:**
    * **`deploy.path`:** _Required_. Indicates the basedir for the release. This is the folder where to deploy the code. (Eg.: `deploy.path/var/www/stylewatch`) 
    * **`build.name`:** _Required_. The name generated for a build. You can retrieve if from the `build` task output. It is the generated filename without the `.tar.gz` extensions.
    * **`host.url`:** _Required_. The host url, with the `http://` or `https://` part (Eg.: `host.url=http://www.stylewatch.com.ar`).
        * **`host.basicauth.username`:** _Optional_. The username for the basic auth.
        * **`host.basicauth.password`:** _Optional_. The password for the basic auth.
    * **`environments`:** _Required_. The environments to deploy to, pipe separated (Example: 'web1|web2|web3'). This must be used in combination with the following parameters:
        * **`[env].ip`:** _Required_. The `ip` for the given `env` (Example: `web1.ip=127.0.0.1`). Note that if you defined two environments, you must pass two ips (Example: `web1.ip=127.0.0.1` `web2.ip=127.0.0.2`)
        * **`[env].port`:** _Optional_. The port to use when connection to `[env].ip`.
        * **`[env].username`:** _Required_. The `username` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].password`:** _Optional_. The `password` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].pem.location`:** _Optional_. The location of the pem file to connect to the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].run.upgrade`:** _Optional_. Indicates if the deploy process should run the `setup:upgrade` command on that particular env. Defaults to `false`.
        * **NOTE:** The script is prepared to receive either `[env].pem.location` or `[env].password`, so if you define the `[env].pem.location` you don't need to define the `[env].password`.
    * **`build.download.ip`:** _Opional_. Indicates the ip to download the build from. If this parameter ommited, then there is no need to define any of the others `build.download.*` parameters.
        * **`build.download.port`:** _Opional_. Indicates the port to use when connecting to `build.download.ip`.
        * **`build.download.path`:** _Opional_. Indicates the path where the build file is located in `build.download.ip`.
        * **`build.download.username`:** _Opional_. Indicates the username to use when connecting to `build.download.ip`.
        * **`build.download.password`:** _Opional_. Indicates the password to use when connecting to `build.download.ip`.
        * **`build.download.pem.location`:** _Opional_. Indicates the location of the pem file to use when connection to `build.download.ip`.
        * **NOTE:** The script is prepared to receive either `build.download.pem.location` or `build.download.password`, so if you define the `build.download.pem.location` you don't need to define the `build.download.password`.
    * **`deploy.additional.sh.script`:** _Optional_. Indicate the path to an additional `bash` file to be executed. Path is relative to the `root folder` of the project. The script will be called with the following paremters:
        * **`--build_name`:** The very same parameter passed to the `deploy` task in `build.name`.
        * **`--deploy_root_dir`:** The very same parameter passed to the `deploy` task in `build.path`.
        * **`--webroot_folder`:** The very same parameter passed to the `deploy` task in `webroot.dir`.
* **`pullOnHosts` parameters:**
    * **`build.branch`:** _Required_. Indicates the branch or tag to pull.
    * **`deploy.path`:** _Required_. Indicates the repository root path on the server side.
    * **`environments`:** _Required_. The environments to deploy to, pipe separated (Example: 'web1|web2|web3'). This must be used in combination with the following parameters:
        * **`[env].ip`:** _Required_. The `ip` for the given `env` (Example: `web1.ip=127.0.0.1`). Note that if you defined two environments, you must pass two ips (Example: `web1.ip=127.0.0.1` `web2.ip=127.0.0.2`)
        * **`[env].port`:** _Optional_. The port to use when connection to `[env].ip`.
        * **`[env].username`:** _Required_. The `username` for the given `env`. Same as before you need to define as many as environments you have defined.
        * **`[env].password`:** _Optional_. The `password` for the given `env`. Same as before you may need to define as many as environments you have defined.
        * **`[env].pem.location`:** _Optional_. The location of the pem file to connect to the given `env`. Same as before you may need to define as many as environments you have defined.
        * **NOTE:** The script is prepared to receive either `[env].pem.location` or `[env].password`, so if you define te `[env].pem.location` you don't need to define the `[env].password`.
* **`wampup_cache` parameters:**
    * **`sitemap.url`:** _Required_. The url of the `sitemap.xml` generated by Magento (Eg.: `sitemap.url=www.stylewatch.com.ar/sitemap.xml`)
* **Other parameters:**
    * **`custom.file`:** _Optional_. Allows to indicate the path to a custom xml file with customizations.

If you run `phing` from the command line, params should be passed in the following way.:

```
#!bash

# Note the -D[param name]
phing build -Dbuild.branch=pre-prod -Dwebroot.dir=webroot
```

### `build` and `build_m2` Tasks ###

This task will create a build package, that will be located in the `devops/builds` folder.

It will run automatically the following tasks:

* Create the `builds` directory.
* Clean the `builds` directory. By default it keeps the last 5 builds, but you can define how much it should keep with the `build.history.files` setting.
* Either compile the css files with `compass` or minify them with `yuicompressor`. You can define one or another via the `build.use.compass` setting (only applies for M1 projects).
    * If you choose to run `compass`, the build process assumes that `compass` is installed globally and that the `config.rb` file is located in the `magento-root-folder`.
* Minify js files with `yuicompressor` (only applies for M1 projects).
* Package the code into a `tar.gz` file ready to be deployed.

#### Examples ####

```
#!bash

phing build -Dbuild.branch=pre-prod -Dwebroot.dir=webroot
phing build -Dbuild.branch=pre-prod -Dwebroot.dir=webroot -Dbuild.number=123
phing build -Dbuild.branch=pre-prod -Dwebroot.dir=webroot -Dbuild.use.compass=true
```

### `deploy` and `deploy_m2` Tasks ###

This task takes a previously created build and deploys it to the defined environments.

It will run automatically the following tasks:
    
* Upload _first_ the file to the different environments.
* Once the deploy was upload to _all_ environments, it will run the deploy itself, that involves:
    * Un-tar the file in the `releases` folder.
    * Delete the tar file.
    * Generate all the symbolic links to the files in the `shared` folder.
    * Generate the symbolic link for the webserver (aka, replace the old `deploy-folder/[webroot folder]` to point to the new release).
    * Clean APC cache.
    * Only for M1. Depending on the `deploy.trigger.cc` parameter it will flush the Magento cache.
    * Only for M2. Depending on the `[env].run.upgrade` parameter it will run the `setup:upgrade` command.

#### Examples ####

```
#!bash

phing deploy -Dwebroot.dir=webroot -Denvironments=web1 \ 
    -Dweb1.ip=127.0.0.1 -Dweb1.username=root -Dweb1.password=123 \
    -Ddeploy.path=/var/www/stylewatch -Dbuild.name=v1.0.0_9

phing deploy -Dwebroot.dir=webroot -Denvironments=web1|web2 \ 
    -Dweb1.ip=127.0.0.1 -Dweb1.username=root -Dweb1.password=123 \
    -Dweb2.ip=127.0.0.2 -Dweb2.username=root -Dweb2.password=456 \
    -Ddeploy.path=/var/www/stylewatch -Dbuild.name=v1.0.0_9
```

### `pullOnHosts` Task ###

This task connects to the given hosts and does a `git pull` of the given branch or tag.

It will run automatically the following tasks:

* Connect to the hosts using the provided `[env].ip`, `[env].username` and `[env].password` (or `[env].pem.location`).
* In the given `deploy.path` runs the following commands:
    * git fetch -q --all;
    * git checkout -q `build.branch`;
    * git pull -q origin `build.branch`;

#### Examples ####

```
#!bash

phing pullOnHosts -Dbuild.branch=pre-prod -Dwebroot.dir=webroot -Denvironments=web1 \ 
    -Dweb1.ip=127.0.0.1 -Dweb1.username=root -Dweb1.password=123 \
    -Ddeploy.path=/var/www/stylewatch -Dhost.url=www.stylewatch.com.ar

phing pullOnHosts -Dbuild.branch=pre-prod -Dwebroot.dir=webroot -Denvironments=web1|web2 \ 
    -Dweb1.ip=127.0.0.1 -Dweb1.username=root -Dweb1.password=123 \
    -Dweb2.ip=127.0.0.2 -Dweb2.username=root -Dweb2.password=456 \
    -Ddeploy.path=/var/www/stylewatch -Dhost.url=www.stylewatch.com.ar
```

### `restore_db` Task ###

This task will assumes that the `app/etc/local.xml` file already exists in place. 
It downloads the database for the project from the Summa Library.
**Attention:** This commands deletes the actual database specified in the `app/etc/local.xml` and imports the new one. 

### `setup_environment` Task ###

This task will assumes that the `app/etc/local.xml` file already exists in place. 
It update some database records accordings to the configuration files in the project.

### `restore_environment` Task ###

This task is a shortcut to run both `restore_db` and `setup_environment`, in that order.

### `warmup_cache` Task ###

This task will retrieve the url to visit from the given sitemap url, and then makes a `curl` call to each one.

#### Examples ####

```
#!bash

phing warmup_cache -Dsitemap.url=www.stylewatch.com.ar/sitemap.xml
```

## Customization ##

If you need to add your own task, or even override some provided ones, you have the possibility via the `custom.file` parameter.
This parameters allows you to specify a path to a xml file (relative to the root repository folder).
This xml file has to be a standard xml file for `phing`, with your own targets.

#### Examples ####

```
#!bash

phing -Dcustom.file=path/to/custom/file.xml [...]
```

## Troubleshooting and Tips ##

### Install composer globally ###

We strongly recommend to install `devops` using composer.
To install composer on you local environment, you can run the following commands:

```
#!bash

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```
