<?php
namespace SummaSolutions;
use Composer\Script\Event;

class DevOps
{
    public static function postInstall(Event $event)
    {
        $event->getIO()->writeError(array(
            'Post install/update script for DevOps are deprecated.',
            'Please remove these lines from the "scripts" section in your composer.json file.',
            '"post-install-cmd": "SummaSolutions\\DevOps::postInstall"',
            '"post-update-cmd": "SummaSolutions\\DevOps::postUpdate"',
        ));
    }

    public static function postUpdate(Event $event)
    {
        static::postInstall($event);
    }
}
